###Welcome to M3::m3core
M3 is an open-source real-time control software provided by Meka Robotics LLC to control the Meka robots. 

> **List of improvements and fixes:**
> - CMake full support
> - Overlays : M3_ROBOT variable now supports multiple paths.
> - Faster python server : up to 250Hz.
> - ROS Indigo support
> - Fake EtherCAT kernel module for virtual installation
> - More Debug info
> - Memory leaks fix

Maintainer : Antoine Hoarau <hoarau.robotics@gmail.com>
>For more information on m3, please visit [mekabot.com/wiki](https://mekabot.com/wiki).